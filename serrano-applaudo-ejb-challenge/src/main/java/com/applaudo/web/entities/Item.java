package com.applaudo.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "item")
@NamedQueries({ @NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i"),
		@NamedQuery(name = "Item.findById", query = "SELECT i FROM Item i WHERE i.id = :id"),
		@NamedQuery(name = "Item.findByItemName", query = "SELECT i FROM Item i WHERE UPPER(i.itemName) = :itemName"),
		@NamedQuery(name = "Item.findAllByItemStatus", query = "SELECT i FROM Item i WHERE i.itemStatus = :itemStatus") })
public class Item implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Size(max = 150)
	@Basic(optional = false)
	@NotNull
	@Column(name = "item_name")
	private String itemName;

	@Basic(optional = false)
	@NotNull
	@Column(name = "item_buying_price")
	private BigDecimal itemBuyingPrice;

	@Basic(optional = false)
	@NotNull
	@Column(name = "item_selling_price")
	private BigDecimal itemSellingPrice;

	@Basic(optional = false)
	@NotNull
	@Column(name = "item_status")
	private Boolean itemStatus;

	@Size(max = 150)
	@Basic(optional = false)
	@NotNull
	@Column(name = "item_entered_by_user")
	private String itemEnteredByUser;

	@Basic(optional = false)
	@NotNull
	@Column(name = "item_entered_date")
	private LocalDateTime itemEnteredDate;

	@Column(name = "item_last_modified_date")
	private LocalDateTime itemLastModifiedDate;

	@Size(max = 150)
	@Column(name = "item_last_modified_by_user")
	private String itemLastModifiedByUser;

	public Item(Long id, @Size(max = 150) @NotNull String itemName, @NotNull BigDecimal itemBuyingPrice,
			@NotNull BigDecimal itemSellingPrice, @NotNull Boolean itemStatus,
			@Size(max = 150) @NotNull String itemEnteredByUser, @NotNull LocalDateTime itemEnteredDate) {
		super();
		this.id = id;
		this.itemName = itemName;
		this.itemBuyingPrice = itemBuyingPrice;
		this.itemSellingPrice = itemSellingPrice;
		this.itemStatus = itemStatus;
		this.itemEnteredByUser = itemEnteredByUser;
		this.itemEnteredDate = itemEnteredDate;
	}

	public Item() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public BigDecimal getItemBuyingPrice() {
		return itemBuyingPrice;
	}

	public void setItemBuyingPrice(BigDecimal itemBuyingPrice) {
		this.itemBuyingPrice = itemBuyingPrice;
	}

	public BigDecimal getItemSellingPrice() {
		return itemSellingPrice;
	}

	public void setItemSellingPrice(BigDecimal itemSellingPrice) {
		this.itemSellingPrice = itemSellingPrice;
	}

	public Boolean getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(Boolean itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getItemEnteredByUser() {
		return itemEnteredByUser;
	}

	public void setItemEnteredByUser(String itemEnteredByUser) {
		this.itemEnteredByUser = itemEnteredByUser;
	}

	public LocalDateTime getItemEnteredDate() {
		return itemEnteredDate;
	}

	public void setItemEnteredDate(LocalDateTime itemEnteredDate) {
		this.itemEnteredDate = itemEnteredDate;
	}

	public LocalDateTime getItemLastModifiedDate() {
		return itemLastModifiedDate;
	}

	public void setItemLastModifiedDate(LocalDateTime itemLastModifiedDate) {
		this.itemLastModifiedDate = itemLastModifiedDate;
	}

	public String getItemLastModifiedByUser() {
		return itemLastModifiedByUser;
	}

	public void setItemLastModifiedByUser(String itemLastModifiedByUser) {
		this.itemLastModifiedByUser = itemLastModifiedByUser;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", itemName=" + itemName + ", itemBuyingPrice=" + itemBuyingPrice
				+ ", itemSellingPrice=" + itemSellingPrice + ", itemStatus=" + itemStatus + ", itemEnteredByUser="
				+ itemEnteredByUser + ", itemEnteredDate=" + itemEnteredDate + ", itemLastModifiedDate="
				+ itemLastModifiedDate + ", itemLastModifiedByUser=" + itemLastModifiedByUser + "]";
	}

}
