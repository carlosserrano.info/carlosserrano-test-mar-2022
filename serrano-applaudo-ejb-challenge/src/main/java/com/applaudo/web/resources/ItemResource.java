package com.applaudo.web.resources;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.applaudo.web.entities.Item;
import com.applaudo.web.services.ItemService;

@RequestScoped
@Path("/item")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ItemResource {

	@Inject
	ItemService itemService;

	@POST
	public Response create(Item item) {
		try {
			if (Objects.nonNull(item.getId()) && item.getId() > 0) {
				Item itemFound = this.itemService.findById(item.getId());
				if (Objects.nonNull(itemFound)) {
					return Response.status(Response.Status.BAD_REQUEST)
							.entity("{\"message\": \"Item already exists in database\"}")
							.type(MediaType.APPLICATION_JSON).build();
				} else {
					this.itemService.save(item);
					item = this.itemService.findById(item.getId());
					return Response.status(Response.Status.CREATED).entity(item).build();
				}
			} else {
				return Response.status(Response.Status.BAD_REQUEST).entity("{\"message\": \"Id is required\"}")
						.type(MediaType.APPLICATION_JSON).build();
			}
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity("{\"message\": \"Id must be numeric\"}")
					.type(MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\"message\": \"Server error\"}")
					.type(MediaType.APPLICATION_JSON).build();
		}

	}
	
	@GET
	@Path("{id}")
	public Response getItem(@PathParam("id") Long id) {
		try {
			if (Objects.nonNull(id) && id > 0) {
				Item itemFound = this.itemService.findById(id);
				if (Objects.nonNull(itemFound)) {
					return Response.status(Response.Status.OK).entity(itemFound).type(MediaType.APPLICATION_JSON)
							.build();
				} else {
					return Response.status(Response.Status.NOT_FOUND)
							.entity("{\"message\": \"Item doesn't exist in database\"}")
							.type(MediaType.APPLICATION_JSON).build();
				}
			} else {
				return Response.status(Response.Status.BAD_REQUEST).entity("{\"message\": \"Id is required\"}")
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity("{\"message\": \"Id must be numeric\"}")
					.type(MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\"message\": \"Server error\"}")
					.type(MediaType.APPLICATION_JSON).build();
		}

	}

	@GET
	@Path("")
	public Response getItemsByNameAndStatus(@QueryParam("itemStatus") Boolean status,
			@QueryParam("itemName") String itemName) {
		try {
			if (Objects.nonNull(status) && Objects.nonNull(itemName)) {
				List<Item> itemsFound = this.itemService.findByItemName(itemName.toUpperCase());

				List<Item> responseList = itemsFound.stream().filter(n -> n.getItemStatus())
						.collect(Collectors.toList());

				responseList.forEach(System.out::println);

				if (itemsFound.size() > 0) {
					return Response.status(Response.Status.OK).entity(responseList).type(MediaType.APPLICATION_JSON)
							.build();
				} else {
					return Response.status(Response.Status.NOT_FOUND)
							.entity("{\"message\": \"Item(s) doesn't exist in database\"}")
							.type(MediaType.APPLICATION_JSON).build();
				}
			} else {
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("{\"message\": \"Status and name are required\"}").type(MediaType.APPLICATION_JSON)
						.build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\"message\": \"Server error\"}")
					.type(MediaType.APPLICATION_JSON).build();
		}

	}

}
