package com.applaudo.web.impls;

import java.time.LocalDateTime;
import java.util.List;
import javax.ejb.Stateless;
import com.applaudo.web.entities.Item;
import com.applaudo.web.services.ItemService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ItemServiceImpl implements ItemService {

	@PersistenceContext(unitName = "restapi_PU")
	EntityManager em;

	@Override
	public List<Item> findAll() {
		return em.createNamedQuery("Item.findAll", Item.class).getResultList();
	}

	@Override
	public Item findById(Long id) {
		return em.find(Item.class, id);
	}

	@Override
	public List<Item> findByItemName(String itemName) {
		return em.createNamedQuery("Item.findByItemName", Item.class).setParameter("itemName", itemName).getResultList();
	}

	@Override
	public List<Item> findAllByItemStatus(Boolean itemStatus) {
		return em.createNamedQuery("Item.findAllByItemStatus", Item.class).getResultList();
	}

	@Override
	public void save(Item item) {
		item.setItemEnteredDate(LocalDateTime.now());
		em.persist(item);
	}

}
