package com.applaudo.web.services;

import java.util.List;

import com.applaudo.web.entities.Item;

public interface ItemService {
	
	List<Item> findAll();

	Item findById(Long id);
	
	List<Item> findByItemName(String itemName);
	
	List<Item> findAllByItemStatus(Boolean itemStatus);

	void save(Item item);

}
