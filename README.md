TO RUN CONTAINER.

APPLICATION RUN

1. In root project folder open a Command Line Prompt (carlosserrano-test-mar-2022\serrano-applaudo-ejb-challenge)
2. Use: mvn package tomee:run
3. Wait until logs: Server startup in 3289 ms
4. Import postman collection named 'carlos-serrano-item-collection.postman_collection'
5. Try postman collectio requests